import pytest
import requests
import json

#unknown resource
def test_list_valid_user(supply_url):
    url = supply_url + "/unknown"
    resp = requests.get(url)
    json_response = resp.json()

    assert json_response['page'] == 1
    assert json_response["data"][1]["id"] == 2
    assert json_response["data"][1]["year"] != 2005