import requests
import json
import jsonpath


def test_add_new_data(supply_url, responseheader, url_to_update_user):
    #newuserAPI
    url = supply_url+"/users"
    f = open("C:\\Users\\amith\\PycharmProjects\\APItesting\\reqjson.json", 'r')
    request_json = json.loads(f.read())
    response = requests.post(url, request_json)
    id = jsonpath.jsonpath(response.json(), 'id')
    xy = response.json()
    value_of_new_id = xy["id"]
    print(xy["id"])
    print(value_of_new_id)
    assert response.status_code == 201


    print("fetching data of just added user")
    url = url_to_update_user+"/"+str(value_of_new_id)
    response_jus = requests.get(url)
    id = jsonpath.jsonpath(response_jus.json(), 'id')
    tokenid = response_jus.json()
    print(tokenid)
    assert response_jus.status_code == 404 #can't access data with the above generated id, hence error


    print("updateuserAPI")
    update_url = url_to_update_user+"/"+str(value_of_new_id)
    f = open("C:\\Users\\amith\\PycharmProjects\\APItesting\\update.json", 'r')
    request_json1 = json.loads(f.read())
    response_put = requests.put(update_url, request_json1)
    assert response_put.status_code == 200  #It should throw error, but problem with the API itself

    # fetching data of just updated user
    url = url_to_update_user+"/"+str(value_of_new_id)
    response_get_upd = requests.get(url)
    id = jsonpath.jsonpath(response.json(), 'id')
    assert response_get_upd.status_code == 404

    # GET method to fetch data
    url = supply_url+"/users"
    response = requests.get(url)
    respo = response.json()
    stored_email = respo["data"][1]["email"]
    print(stored_email)
    print(respo["data"][1]["id"])
    assert response.status_code == 200


    # REGISTER
    url_register = supply_url+"/register"
    payload = {
    "email": stored_email,
    "password":"123xyz"
    }
    response_reg = requests.post(url_register, payload)
    tokenid = response_reg.json()
    print(tokenid["id"])
    assert response_reg.status_code == 200

    # Login
    url_register = supply_url+"/login"
    payload_login = {
    "email": stored_email,
    "password":"123xyz"
    }
    response_log = requests.post(url_register, payload_login)
    tokenid = response_log.json()
    print(tokenid["token"])
    assert response_log.status_code == 200




