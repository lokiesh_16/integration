import pytest
import requests
import json


def test_create_user(supply_url):
    url = supply_url + "/users"
    f = open("C:\\Users\\amith\\PycharmProjects\\APItesting\\reqjson.json", 'r')
    request_json = json.loads(f.read())

    resp = requests.post(url, data=request_json)
    json_response = resp.json()
    assert resp.status_code == 201, resp.text
    print(json.dumps(resp.json(), indent=4))

def test_valid_login(supply_url):
    url = supply_url + "/login/"
    f = open("C:\\Users\\amith\\PycharmProjects\\APItesting\\login.json", 'r')
    login_cred = json.loads(f.read())
    resp = requests.post(url, data=login_cred)
    json_response = resp.json()
    assert resp.status_code == 200, 'login credentials are wrong if status code is not equal to 200'



