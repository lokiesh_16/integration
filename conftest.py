import pytest


@pytest.fixture
def supply_url():
    return "https://reqres.in/api"


@pytest.fixture
def url_to_update_user():
    return "https://reqres.in/api/users"


@pytest.fixture
def responseheader():
    return "application/json; charset=utf-8"

