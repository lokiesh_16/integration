import pytest
import requests
import json



def test_list_valid_user(supply_url):
    url = supply_url + "/users/2"
    resp = requests.get(url)
    json_response = resp.json()
    assert json_response["data"]["id"] == 2

    assert json_response["data"]["first_name"] != "John"